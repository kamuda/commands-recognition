function [Spectrograms] = speechSpectrograms(ads)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

frameDuration = 0.025;
hopDuration = 0.010;
numBands = 40;
epsil = 1e-6;
Spectrograms = cell(numel(ads.Labels), 1);


for i = 1: numel(ads.Labels)
    [audioIn,fs] = audioread(ads.Files{i});
    if length(audioIn) > fs
        audioIn = audioIn(1:fs-10);
    end
    assert(length(audioIn) <= fs, 'file %s is longes than 1s\n', ads.Files{i}) 
    audio = (rand(fs, 1)- 0.5)/1e4;
    diff = fs - length(audioIn);
    index_start = round(rand() * (diff-5) + 1);
    index_stop = index_start + length(audioIn) - 1;
    audio(index_start:index_stop) = audioIn * (rand()+0.5);
    OverlapLength = round(fs*(frameDuration - hopDuration));
    S = melSpectrogram(audio, fs, ...s
                   'WindowLength', round(fs*frameDuration),...
                   'OverlapLength', round(OverlapLength), ...
                   'FFTLength',ceil(fs/40), ...
                   'NumBands', round(numBands),...
                   'FrequencyRange',[62.5,8e3]);
    Spectrograms{i} = log10(S + epsil);           
     if(mod(i, 100) == 0)
         fprintf('Processed %7d files out of %d\n', i, numel(ads.Labels));
     end
    assert(length(Spectrograms{i}) == 98,'file %s shape is not 40x98\n', ads.Files{i} )
end

end

