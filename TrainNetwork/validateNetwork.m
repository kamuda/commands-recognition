%% Valildate Network performance

YTestPred = classify(trainedNet,XTest2);
testError = mean(YTestPred ~= YTest);
YTrainPred = classify(trainedNet,XTrain2);
trainError = mean(YTrainPred ~= YTrain);
disp("Training error: " + trainError*100 + "%")
disp("Test error: " + testError*100 + "%")

%% Plot the confusion matrix. 
%  Display the precision and recall for each class by using column 
%  and row summaries. Sort the classes of the confusion matrix. 
%  The largest confusion is between unknown words and commands, 
%  up and off, down and no, and go and no.

figure('Units','normalized','Position',[0.2 0.2 0.5 0.5]);
cm = confusionchart(YTest,YTestPred);
cm.Title = 'Confusion Matrix for Test Data';
cm.ColumnSummary = 'column-normalized';
cm.RowSummary = 'row-normalized';
sortClasses(cm, [commands,"unknown","background"])
