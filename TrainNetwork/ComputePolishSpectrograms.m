%% Load Speech Commands Data Set
%  Download the data set from 
%  http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz 
%  and extract the downloaded file

dataFolder = '../database'
ads = audioDatastore(dataFolder, ...
    'IncludeSubfolders',true, ...
    'FileExtensions','.wav', ...
    'LabelSource','foldernames')

%% Choose Words to Recognize
%

% commands = categorical(["down","eight","five","four","go","left","nine","no","off","on","one","right","seven","six","stop","three","two","up","yes","zero"]);
% 
% isCommand = ismember(ads.Labels,commands);
% isUnknown = ~ismember(ads.Labels,[commands,"_background_noise_"]);
% 
% includeFraction = 0.3;
% mask = rand(numel(ads.Labels), 1) < includeFraction;
% isUnknown = isUnknown & mask;
% ads.Labels(isUnknown) = categorical("unknown");
% 
% ads = subset(ads,isCommand|isUnknown);
countEachLabel(ads)
%% Split data
% split the data into a test set and a learning set 

[trainData, testData] = splitData(ads, 0.25)
[testData, validateData] = splitData(testData, 0.5)
countEachLabel(trainData)
countEachLabel(validateData)
countEachLabel(testData)



%% Compute Speech Spectrograms


XTrain = speechSpectrograms(trainData);
XTest = speechSpectrograms(testData);
XValidate = speechSpectrograms(validateData);





