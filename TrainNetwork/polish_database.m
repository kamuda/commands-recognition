dataFolder = '../database/'
ads = audioDatastore(dataFolder, ...
    'IncludeSubfolders',true, ...
    'FileExtensions','.wav', ...
    'LabelSource','foldernames')


%%


frameDuration = 0.025;
hopDuration = 0.010;
numBands = 40;
epsil = 1e-6;
Spectrograms = cell(numel(ads.Labels), 1);


for i = 1 : numel(ads.Labels)
    [audioIn,fs] = audioread(ads.Files{i});
    OverlapLength = round(fs*(frameDuration - hopDuration));
    S = melSpectrogram(audioIn(:,1),fs, ...
                   'WindowLength',round(fs*frameDuration),...
                   'OverlapLength', round(OverlapLength), ...
                   'FFTLength',round(fs/40), ...
                   'NumBands', numBands,...
                   'FrequencyRange',[62.5,8e3]);
    size(S)
    Spectrograms{i} = log10(S + epsil);           
    if(mod(i, 100) == 0)
        fprintf('Processed %7d files out of %d\n', i, numel(ads.Labels));
    end
end


