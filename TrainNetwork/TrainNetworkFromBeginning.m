%% Load or compute spectrograms
%  load or compute spectrograms - it may took a while 
%  loadig spectrograms:
% load ../../ang_spectrograms.mat

%  computing spectrograms: 
%  Note: this option requires dataset that is available here: 
%  http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz
% computeSpectrograms
ComputePolishSpectrograms;
display('loading spectrograms done')

%% Spectrograms cells to array

XTest2 = ones(40, 98, 1, numel(XTest)) * -6;
for i = 1:numel(XTest)
   [x, y] = size(XTest{i});
   XTest2(1:x,1:y,1,i) = XTest{i};
       
end

XTrain2 = ones (40, 98, 1, numel(XTrain)) * -6;
for i = 1:numel(XTrain)
   [x, y] = size(XTrain{i});
   XTrain2(1:x,1:y,1,i) = XTrain{i};
       
end

XValidate2 = ones (40, 98, 1, numel(XValidate)) * -6;
for i = 1:numel(XValidate)
   [x, y] = size(XValidate{i});
   XValidate2(1:x,1:y,1,i) = XValidate{i};
       
end



clear XTest
clear XTrain
clear XValidate
display('Cells to arrys done')


%% Visualize Data
%  Plot the waveforms and spectrograms of a few training examples. Play the corresponding audio clips.

specMin = min(XTrain2(:));
specMax = max(XTrain2(:));
idx = randperm(size(XTrain2,4),3);
figure('Units','normalized','Position',[0.2 0.2 0.6 0.6]);
for i = 1:3
    [x,fs] = audioread(trainData.Files{idx(i)});
    subplot(2,3,i)
    plot(x)
    axis tight
    title(string(trainData.Labels(idx(i))))

    subplot(2,3,i+3)
    spect = XTrain2(:,:,1,idx(i));
    pcolor(spect)
    caxis([specMin+2 specMax])
    shading flat

    sound(x,fs)
    pause(2)
end
disp('visuaization done')

%% plot the histogram of training data
figure
histogram(XTrain2,'EdgeColor','none','Normalization','pdf')
axis tight
ax = gca;
ax.YScale = 'log';
xlabel("Input Pixel Value")
ylabel("Probability Density")
title('Training data pixels histogram')
disp('ploting histograms done')
%% get Y labels

YTrain = trainData.Labels;
YTest = testData.Labels;
YValidate = validateData.Labels;

YTrain = removecats(YTrain);
YTest = removecats(YTest);
YValidate = removecats(YValidate);

disp('Adding backgroud noise to datasets done')



%% clear mem;
% close all;
% clear y;
% clear x;
% clear BkgSpectrograms;
% clear dataFolder;
% clear isCommand;
% clear isUnknown;
% clear mask;
% 
% clear nums;
% clear specx;
%% Data argumenataion
epsil= 1e-6;
sz = size(XTrain2);
specSize = sz(1:2);
imageSize = [specSize 1];
augmenter = imageDataAugmenter( ...
    'RandXTranslation',[-10 10], ...
    'RandXScale',[0.8 1.2], ...
    'FillValue',log10(epsil));
augimdsTrain = augmentedImageDatastore(imageSize,XTrain2,YTrain, ...
    'DataAugmentation',augmenter);
augimdsValidate = augmentedImageDatastore(imageSize,XValidate2,YValidate, ...
    'DataAugmentation',augmenter);
augimdsTest = augmentedImageDatastore(imageSize,XTest2,YTest, ...
    'DataAugmentation',augmenter);

%% Define Neural Network Architecture
defineNetoworkArchitecture

%% Train network

miniBatchSize = 128;
validationFrequency = floor(numel(YTrain)/miniBatchSize);
options = trainingOptions('adam', ...
    'InitialLearnRate',3e-4, ...
    'MaxEpochs',25, ...
    'MiniBatchSize',miniBatchSize, ...
    'Shuffle','every-epoch', ...
    'Plots','training-progress', ...
    'ValidationData',{XValidate2,YValidate}, ...
    'Verbose',false, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropFactor',0.1, ...
    'LearnRateDropPeriod',20);

trainedNet = trainNetwork(augimdsTrain,layers,options);

%% Valildate Network performance

YTestPred = classify(trainedNet,XTest2);
testError = mean(YTestPred ~= YTest);
YValidatePred = classify(trainedNet,XValidate2);
validateError = mean(YValidatePred ~= YValidate);
YTrainPred = classify(trainedNet,XTrain2);
trainError = mean(YTrainPred ~= YTrain);
disp("Training error: " + trainError*100 + "%")
disp("Validate error: " + validateError*100 + "%")
disp("Test error: " + testError*100 + "%")

%% Plot the confusion matrix. 
%  Display the precision and recall for each class by using column 
%  and row summaries. Sort the classes of the confusion matrix. 
%  The largest confusion is between unknown words and commands, 
%  up and off, down and no, and go and no.

figure('Units','normalized','Position',[0.2 0.2 0.5 0.5]);
cm = confusionchart(YTest,YTestPred);
cm.Title = 'Confusion Matrix for Test Data';
cm.ColumnSummary = 'column-normalized';
cm.RowSummary = 'row-normalized';
sortClasses(cm, [commands,"unknown","background"])