function [trainData, testData] = splitData(ads, test_propability)
%SPLITDATA Summary of this function goes here
%   Detailed explanation goes here

assert(test_propability < 1)
assert(test_propability > 0)

isTest = rand(numel(ads.Labels), 1) < test_propability;

testData = subset(ads, isTest);
trainData = subset(ads, ~isTest);

end

