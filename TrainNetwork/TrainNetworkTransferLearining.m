%% load data
ComputePolishSpectrograms


%% Spectrograms cells to array

XTest2 = ones(40, 98, 1, numel(XTest)) * -6;
for i = 1:numel(XTest)
   [x, y] = size(XTest{i});
   XTest2(1:x,1:y,1,i) = XTest{i};
       
end

XTrain2 = ones (40, 98, 1, numel(XTrain)) * -6;
for i = 1:numel(XTrain)
   [x, y] = size(XTrain{i});
   XTrain2(1:x,1:y,1,i) = XTrain{i};
       
end

XValidate2 = ones (40, 98, 1, numel(XValidate)) * -6;
for i = 1:numel(XValidate)
   [x, y] = size(XValidate{i});
   XValidate2(1:x,1:y,1,i) = XValidate{i};
       
end



clear XTest
clear XTrain
clear XValidate
display('Cells to arrys done')


%% plot the histogram of training data
figure
histogram(XTrain2,'EdgeColor','none','Normalization','pdf')
axis tight
ax = gca;
ax.YScale = 'log';
xlabel("Input Pixel Value")
ylabel("Probability Density")

disp('ploting histograms done')
%% get Y labels

YTrain = trainData.Labels;
YTest = testData.Labels;
YValidate = validateData.Labels;

YTrain = removecats(YTrain);
YTest = removecats(YTest);
YValidate = removecats(YValidate);

disp('Adding backgroud noise to datasets done')
%% load pretrained network
addpath('../LoadTrainedNetworks')
net = loadNet();

lgraph = layerGraph(net.Layers);
numClasses = numel(categories(YTrain));

newFCLayer = fullyConnectedLayer(numClasses,'Name','new_fc','WeightLearnRateFactor',10,'BiasLearnRateFactor',10);
lgraph = replaceLayer(lgraph,'fc',newFCLayer);

classWeights = 1./countcats(YTrain);
classWeights = classWeights'/mean(classWeights);
newClassLayer = weightedClassificationLayer(classWeights)
newClassLayer.Name = 'New_classoutput'
lgraph = replaceLayer(lgraph,'classoutput',newClassLayer);
%%

epsil= 1e-6;
sz = size(XTrain2);
specSize = sz(1:2);
imageSize = [specSize 1];
augmenter = imageDataAugmenter( ...
    'RandXTranslation',[-10 10], ...
    'RandXScale',[0.8 1.2], ...
    'FillValue',log10(epsil));
augimdsTrain = augmentedImageDatastore(imageSize,XTrain2,YTrain, ...
    'DataAugmentation',augmenter);
augimdsValidate = augmentedImageDatastore(imageSize,XValidate2,YValidate, ...
    'DataAugmentation',augmenter);
augimdsTest = augmentedImageDatastore(imageSize,XTest2,YTest, ...
    'DataAugmentation',augmenter);

%%
options = trainingOptions('sgdm', ...
    'MiniBatchSize',10, ...
    'MaxEpochs',8, ...
    'InitialLearnRate',1e-4, ...
    'Shuffle','every-epoch', ...
    'ValidationData',augimdsValidate, ...
    'ValidationFrequency',5, ...
    'Verbose',false, ...
    'Plots','training-progress');

transferTrainedNet = trainNetwork(augimdsTrain,lgraph,options);

%% Valildate Network performance

YTestPred = classify(transferTrainedNet,XTest2);
testError = mean(YTestPred ~= YTest);
YValidatePred = classify(transferTrainedNet,XValidate2);
validateError = mean(YValidatePred ~= YValidate);
YTrainPred = classify(transferTrainedNet,XTrain2);
trainError = mean(YTrainPred ~= YTrain);
disp("Training error: " + trainError*100 + "%")
disp("Validate error: " + validateError*100 + "%")
disp("Test error: " + testError*100 + "%")

%% Plot the confusion matrix. 
%  Display the precision and recall for each class by using column 
%  and row summaries. Sort the classes of the confusion matrix. 
%  The largest confusion is between unknown words and commands, 
%  up and off, down and no, and go and no.

figure('Units','normalized','Position',[0.2 0.2 0.5 0.5]);
cm = confusionchart(YTest,YTestPred);
cm.Title = 'Confusion Matrix for Test Data';
cm.ColumnSummary = 'column-normalized';
cm.RowSummary = 'row-normalized';
sortClasses(cm, [commands,"unknown","background"])