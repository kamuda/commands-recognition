%% load trained neural network

loadNet();
disp('Loaded neural network is able to recognize comands listed bellow:')
disp(trainedNet.Layers(end).Classes)

%% Record audio
fs = 16e3;
recObj = audiorecorder(fs,16, 1);

disp('start speaking')
recordblocking(recObj, 1);
disp('recordinf finished')
X = getaudiodata(recObj);
sound(X, fs)


% Recognise recored audio
spectrogram = computeSpectrogram(X, fs);

S = reshape(spectrogram, 40, 98, 1, 1);
classify(trainedNet,S)
figure()
subplot(2, 1, 1)
plot((1:numel(X))/fs, X)
title('Recorded audio')
subplot(2, 1, 2)
pcolor(S)
caxis([-6, 3])
shading flat
title('Spectrogram')



