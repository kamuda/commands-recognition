function [spectrogram] = computeSpectrogram(x, fs)
%COMPUTESPECTROGRAM Summary of this function goes here
%   Detailed explanation goes here

assert(size(x, 1) < fs, 'word is to long')

if(size(x, 1) < fs)
    x = [x;zeros(fs - size(x,1),1)];
end

assert(size(x, 1) == fs, 'length of sound must be 1s long')

frameDuration = 0.025;
hopDuration = 0.010;
numBands = 40;
epsil = 1e-6;

spectrogram = zeros(40, 98, 1);
OverlapLength = round(fs*(frameDuration - hopDuration));
S = melSpectrogram(x,fs, ...
               'WindowLength',round(fs*frameDuration),...
               'OverlapLength', OverlapLength, ...
               'FFTLength',1200, ...
               'NumBands', numBands,...
               'FrequencyRange',[62.5,8e3]);
spectrogram(:, :, 1) = log10(S + epsil);           


end
