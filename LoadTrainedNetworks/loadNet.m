function trainedNet = loadNet()
    classWeights = [1.0592205608962701 1.0414880738276941 1.0426517476532 1.0305613629482211 ...
                    1.0391685012801937 1.0420695858733826 1.0538377347821728 ...
                    1.0473325635798139 1.0574201860052284 1.0444021423051082 ...
                    1.044986913941337 1.0556259209837264 1.0520555965610079 ...
                    1.0368592379440156 1.0598220490058081 1.066483787599559 ...
                    1.033987051689323 1.0299926204741876 1.0432345602566955 ...
                    1.0622348482067321 0.47333163284281715 0.5832333213435088 ...
                    ];

    dropoutProb = 0.2;

    epsil = 1E-6;

    imageSize = [40 98 1];

    includeFraction = 0.3;

    numClasses = 22;

    numF = 12;

    timePoolSize = 13;

    myClasificationLayer = weightedClassificationLayer(classWeights);

    layers = [
        imageInputLayer(imageSize)

        convolution2dLayer(3,numF,'Padding','same')
        batchNormalizationLayer
        reluLayer

        maxPooling2dLayer(3,'Stride',2,'Padding','same')

        convolution2dLayer(3,2*numF,'Padding','same')
        batchNormalizationLayer
        reluLayer

        maxPooling2dLayer(3,'Stride',2,'Padding','same')

        convolution2dLayer(3,4*numF,'Padding','same')
        batchNormalizationLayer
        reluLayer

        maxPooling2dLayer(3,'Stride',2,'Padding','same')

        convolution2dLayer(3,4*numF,'Padding','same')
        batchNormalizationLayer
        reluLayer
        convolution2dLayer(3,4*numF,'Padding','same')
        batchNormalizationLayer
        reluLayer

        maxPooling2dLayer([1 timePoolSize])

        dropoutLayer(dropoutProb)
        fullyConnectedLayer(numClasses)
        softmaxLayer
        myClasificationLayer];

load('myNet.mat');
