function trainedNet = loadPolishNet()

classWeights = [1.2250201533891714 1.0705131070157625 0.82518718665798352 ...
                0.9319761166960755 0.91055137838122313 1.0199738616201686 ...
                0.87695169652213745 0.94682832572708864 0.89343575096804229 ...
                1.0515659723783153 0.99022462398958022 1.0423417094627161 ...
                0.973991433432374 1.3202994986527736 0.92113918510658621 ...
                ];


dropoutProb = 0.2;

epsil = 1E-6;

imageSize = [40 98 1];

saveVarsMat = load('loadPolishNet.mat');

layers = saveVarsMat.layers; % <24x1 nnet.cnn.layer.Layer> unsupported class

miniBatchSize = 128;

myClasificationLayer = saveVarsMat.myClasificationLayer; % <1x1 weightedClassificationLayer> unsupported class

options = saveVarsMat.options; % <1x1 nnet.cnn.TrainingOptionsADAM> unsupported class

trainedNet = saveVarsMat.trainedNet; % <1x1 SeriesNetwork> unsupported class

clear saveVarsMat;
