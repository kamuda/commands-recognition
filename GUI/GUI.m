function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 28-May-2019 23:17:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;


addpath("../LoadTrainedNetworks")

handles.Fs=44100;
handles.net = loadPolishTransferNet;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editSLength_Callback(hObject, eventdata, handles)
% hObject    handle to editSLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSLength as text
%        str2double(get(hObject,'String')) returns contents of editSLength as a double


% --- Executes during object creation, after setting all properties.
function editSLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_StartRec.
function pushbutton_StartRec_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_StartRec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.pushbutton_StartRec.String == "Start recording")
    handles.pushbutton_StartRec.String = "Stop recording";
    startrecording(hObject, handles);
    set(handles.text_wordNo,'Visible','off')
    set(handles.text4,'Visible','off')
    set(handles.axes_signal,'Visible','off')
    cla(handles.axes_signal)
    set(handles.pushbutton_saveSignal,'Visible','off')
    set(handles.pushbutton_play,'Visible','off')
    set(handles.pushbutton_spectro,'Visible','off')
    set(handles.text_FileName,'Visible','off')
    set(handles.edit_FileName,'Visible','off')
    set(handles.text_words,'Visible','off')
    set(handles.text_calc,'Visible','off')
    set(handles.pushbutton_recognize,'Visible','off')
else
    handles.pushbutton_StartRec.String = "Start recording";
    stoprecording(hObject, handles);
    set(handles.text_wordNo,'Visible','on')
    set(handles.text4,'Visible','on')
    set(handles.axes_signal,'Visible','on')
    set(handles.pushbutton_saveSignal,'Visible','on')
    set(handles.pushbutton_play,'Visible','on')
    set(handles.pushbutton_spectro,'Visible','on')
    set(handles.text_FileName,'Visible','on')
    set(handles.edit_FileName,'Visible','on')
    set(handles.pushbutton_recognize,'Visible','on')
end




function startrecording(hObject, handles)
    handles.recObj = audiorecorder(handles.Fs,16,1);
    record(handles.recObj);
    guidata(hObject, handles);


function stoprecording(hObject, handles)
    stop(handles.recObj)
    handles.signal = getaudiodata(handles.recObj);
    handles.signal = handles.signal(0.3*handles.Fs:end); %Obci�cie kawa�ka na pocz�tku ze wzgl�du na b. du�y szum
    handles.words = preprocessAudio(hObject, handles);
    guidata(hObject, handles);

    
function words = preprocessAudio(hObject, handles)
y = handles.signal;
fs = handles.Fs;
t = 1/fs:1/fs:length(y)/fs;

env = imdilate(abs(y), true(11500, 1));
nonQuietParts = env > 0.04;
beginning = strfind(nonQuietParts',[0 1]);
ending = strfind(nonQuietParts', [1 0]);

if(length(ending) < length(beginning))
    ending(end+1) = length(env);
end

idx=[];
for i=1:length(beginning)-1
    if(t(beginning(i+1)) - t(ending(i)) < 0.01) % sta�a dobrana eksperymentalnie, na pewno < 0.03
        idx(end+1) = i;
    end
end
ending(idx) = [];
beginning(idx+1)=[];

words = cell(length(beginning),1);

for i=1:length(beginning)
    words{i} = y(beginning(i):ending(i));
end

set(handles.text_wordNo,'String', length(words));

plot(handles.axes_signal, t, y)
hold on
plot(handles.axes_signal, t,env)
for i=1:length(beginning)
    line(handles.axes_signal, [t(beginning(i)) t(beginning(i))], get(gca, 'ylim'), 'Color', 'red');
    line(handles.axes_signal, [t(ending(i)) t(ending(i))], get(gca, 'ylim'), 'Color', 'red');
end
xlabel(handles.axes_signal, 'Time (s)');
ylabel(handles.axes_signal, 'Amplitude');
title(handles.axes_signal, 'Recorded audio signal - separate words marked with vertical lines');
zoom on;


function plotSpektro(hObject, handles)
figure
% spectrogram(handles.signal, handles.Fs/4, 11000, [], handles.Fs);
spectrogram(handles.signal, 4096, 4000, [], handles.Fs);
title('Spectrogram of recorded audio');
zoom on;


% --- Executes on button press in pushbutton_saveSignal.
function pushbutton_saveSignal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveSignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

label = handles.edit_FileName.String;
path = ['..\database\', label];

if ~exist(path, 'dir')
    mkdir(path)
end

for i=1:length(handles.words)
    c = clock;
    cS = mat2str(c);
    cS=cS((2:end-5));
    cS(cS == ' ') = '_';
    filename = [cS, '_', num2str(i), '_', label];

    audiowrite([path, '\', filename, '.wav'], handles.words{i}, handles.Fs);
end


function edit_FileName_Callback(hObject, eventdata, handles)
% hObject    handle to edit_FileName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_FileName as text
%        str2double(get(hObject,'String')) returns contents of edit_FileName as a double


% --- Executes during object creation, after setting all properties.
function edit_FileName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_FileName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes_signal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_signal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_signal


% --- Executes on button press in pushbutton_play.
function pushbutton_play_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sound(handles.signal, handles.Fs);



% --- Executes during object creation, after setting all properties.
function axes_spectro_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_spectro (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_spectro


% --- Executes on button press in pushbutton_recognize.
function pushbutton_recognize_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_recognize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(length(handles.words) < 1)
    words = 'No words to recognize';
    equation = [];
else
    try
        labels = classifyWords(handles.net, handles.words, handles.Fs);
        words = [];
        for i=1:length(labels)
            words = [words, labels{i}];
        end
        [mathStr, res] = strToMath(labels);
        equation = [mathStr, '=', num2str(res)];
    catch
        words = 'Unhandled exception occurred';
        equation = [];
    end
end
set(handles.text_words,'String', words);
set(handles.text_calc,'String', equation);
set(handles.text_words,'Visible','on')
set(handles.text_calc,'Visible','on')
guidata(hObject, handles);


% --- Executes on button press in pushbutton_spectro.
function pushbutton_spectro_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_spectro (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
plotSpektro(hObject, handles)
