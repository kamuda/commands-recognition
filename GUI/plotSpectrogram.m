function [ret] = plotSpectrogram(audio, fs)
%PLOTSPECTROGRAM Summary of this function goes here
%   Detailed explanation goes here

frameDuration = 0.025;
hopDuration = 0.010;
numBands = 40;
epsil = 1e-6;

OverlapLength = round(fs*(frameDuration - hopDuration));
S = melSpectrogram(audio, fs, ...s
                   'WindowLength', round(fs*frameDuration),...
                   'OverlapLength', round(OverlapLength), ...
                   'FFTLength',ceil(fs/40), ...
                   'NumBands', round(numBands),...
                   'FrequencyRange',[62.5,8e3]);
Spectrogram = log10(S + epsil);
subplot(2, 1, 1)
plot((1:length(audio))/fs, audio)
v = axis;
v(2) = length(audio)/fs;
axis(v)
title('Audio In')
subplot(2, 1, 2)
pcolor(Spectrogram)
shading flat
title('Spectrogram')






ret = 0;
end

