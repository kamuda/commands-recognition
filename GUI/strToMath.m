function [symbols, result] = strToMath(words)
    % string to math

%     words={'jeden', 'dwa', 'razy', 'trzy', 'wynik'};

    % symbols = cell(length(words),1);
    symbols=[];
    flag = 0;

    for i=1:length(words)
       word = words{i};

       switch word
           case 'jeden'
               symbol = '1';
           case 'dwa'
               symbol = '2';
           case 'trzy'
               symbol = '3';
           case 'cztery'
               symbol = '4';
           case {'piec', 'pi��'}
               symbol = '5';
           case {'szesc', 'sze��'}
               symbol = '6';
           case 'siedem'
               symbol = '7';
           case 'osiem'
               symbol = '8';
           case {'dziewiec', 'dziewi��'}
               symbol = '9';
           case 'zero'
               symbol = '0';
           case 'razy'
               symbol = '*';
           case 'plus'
               symbol = '+';
           case 'minus'
               symbol = '-';
           case 'przez'
               symbol = '/';
           case 'wynik'
               symbol = '=';
               flag = 1;
           otherwise
               flag = 1;
       end

       if(flag)
           break
       end

       symbols = strcat(symbols, symbol);
    %    symbols{i} = symbol;

    end

    % number = [];
    % 
    % numbers = [];
    % j = 0;
    % for i = 1:length(symbols)
    %    if (ismember(symbols{i}, ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']))
    %        number = [number, symbols{i}];
    %    else
    %        numbers(j) = str2double(number);
    %        number = [];
    %        j=j+1;
    %    end
    % end

    result = str2num(symbols);