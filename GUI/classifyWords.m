function labels = classifyWords(trainedNet, words, fs)

    labels = cell(length(words),1);
    for i=1:length(words)
        word = words{i};
        spectrogram = computeOneSpectrogram(word, fs);
        S = reshape(spectrogram, 40, 98, 1, 1);
        labels{i} = classify(trainedNet,S);
    end
%     figure()
%     subplot(2, 1, 1)
%     plot((1:numel(X))/fs, X)
%     title('Recorded audio')
%     subplot(2, 1, 2)
%     pcolor(S)
%     caxis([-6, 3])
%     shading flat
%     title('Spectrogram')
end